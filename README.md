# Bitcoin Prediction

This project is aimed at predicting the bitcoin value with 3 statistical techniques : ARIMA modelling, multivariable regression and RNN.

## Group

Hugo Chan-To-Hing

Christophe Doustaly

Alexandre Herment

Bruno Tabet

## Project Organization

    ├── README.md          <- Contains desctiption of the project
    ├── notebooks           <- folder for notebooks
        ├── statistical_analysis.ipynb <- notebook for ARIMA modeling, and multivariable regression
        ├── deep_learning_analysis.ipynb <- notebook for RNN modeling
    ├── data               <- folder for data
        ├── df_blockchain.csv <- bitcoin and blockchain data from https://blockchain.info/
    ├── .gitignore
